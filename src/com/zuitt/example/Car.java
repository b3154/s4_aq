package com.zuitt.example;

public class Car {
    //Properties/Attributes - the characteristics of the object the class will create
    //Constructor - method to create the object and instantiate with its initialized value
    //Getters and Setters - are methods to get values of an objects properties or set them
    //Methods - actions that an object can perform or do.

    //public access - the variable/property in the class is accessible anywhere in the application
    //private - limits the access and ability to get or set a variable/method to only within its own class
    //getters - methods that return the value of the property
    //setters - methods that allow us to set the value of a property
    private String make;

    private String brand;

    private int price;

    private Driver carDriver;

    //Constructor is a method which allows us to set the initial value of an instance
    public Car(){
        this.carDriver = new Driver();
    }

    public Car(String make,String brand, int price, Driver driver){
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    //Getter and Setter for our properties
    //Getters return a value so, therefor we must add the data type of the value returned

    public String getMake(){
        //this keyword refers to the object. instance where the constructor or setter/getter is
        return this.make;
    }

    public void setMake(String makeParams){
        this.make = makeParams;
    }

    public String getBrand(){
        //this keyword refers to the object. instance where the constructor or setter/getter is
        return this.brand;
    }

    public void setBrand(String brandParams){
        this.brand = brandParams;
    }

    public int getPrice(){
        //this keyword refers to the object. instance where the constructor or setter/getter is
        return this.price;
    }

    public void setPrice(int priceParams){
        this.price = priceParams;
    }


    //Classes have relationship
    //Composition allows modelling objects to be made up for other objects.
    //Classes can have new instances of other clases

    public Driver getCarDriver(){
        return  carDriver;
    }

    public void setCarDriver(Driver newDriver){

         this.carDriver = carDriver;
    }


    //custom method to retrieve the car driver's name

    public String getDriverName(){
        return this.carDriver.getName();
    }

    //Methods are functions of an object/instance which allows to perform certain tasks.
    public void start(){
        System.out.println("Broom Broom");
    }

}
