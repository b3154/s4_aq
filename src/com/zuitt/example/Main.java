package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 200000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);

            Driver driver1 = new Driver("Alejandro", 25);
          // System.out.println(driver1.name);
            car1.start();


            System.out.println(car1.getMake());

            car1.setMake("Veyron");
            System.out.println(car1.getMake());

            //carDriver Getter
            System.out.println(car1.getCarDriver().getName());
            Driver newDriver = new Driver("Antonio", 23);
            car1.setCarDriver(newDriver);

            //Get name of new carDriver
            System.out.println(car1.getCarDriver().getName());
            System.out.println(car1.getCarDriver().getAge());
            System.out.println(car1.getDriverName());


            Animal animal1 = new Animal("Clifford","Red");
            animal1.call();

            Dog dog1 = new Dog();
            System.out.println(dog1.getName());
            dog1.call();;
            dog1.setName("Hachiko");
            System.out.println(dog1.getName());
            dog1.call();
            dog1.setColor("Brown");
            System.out.println(dog1.getColor());

    }
}
